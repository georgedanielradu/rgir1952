package salariati.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(String cnp, String firstName, String lastName, String age, String salary) {
		Employee oldEmployee = employeeRepository.getEmployeeByCnp(cnp);
		if (firstName.trim().equals("")) firstName = oldEmployee.getFirstName();
		if (lastName.trim().equals("")) lastName = oldEmployee.getLastName();
		if (age.trim().equals("")) age = oldEmployee.getAge();
		if (salary.trim().equals("")) salary = oldEmployee.getSalary();
		Employee newEmployee = new Employee(firstName, lastName, age, cnp, oldEmployee.getFunction(), salary);
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	public void deleteEmployee(String cnp) {
		employeeRepository.deleteEmployee(employeeRepository.getEmployeeByCnp(cnp));
	}

	public List<Employee> sortEmployees() {
		List<Employee> employees = employeeRepository.getEmployeeList();
		Collections.sort(employees, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				int salaryComparer = o2.getSalary().compareTo(o1.getSalary());
				if(salaryComparer == 0){
					return o2.getCnp().substring(1, 6).compareTo(o1.getCnp().substring(1, 6));
					//return o2.getAge().compareTo(o1.getAge());
				}
				return salaryComparer;
			}
		});
		return employees;
	}
	
}

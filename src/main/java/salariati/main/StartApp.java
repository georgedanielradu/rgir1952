package salariati.main;

import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

import java.io.Console;
import java.util.List;
import java.util.Scanner;

//functionalitati
//i.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//ii.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//iii.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
public class StartApp {

	public static void mainMenu(EmployeeController employeeController){
		while (true) {
			System.out.println("Menu:");
			System.out.println("1 add");
			System.out.println("2 Delete");
			System.out.println("3 Update");
			System.out.println("4 Sort");
			System.out.println("5 Show");
			System.out.println("0 Exit");
			System.out.println("Enter Command : ");

			Scanner sc = new Scanner(System.in);
			int command = sc.nextInt();
			switch (command) {
				case 1:
					add(employeeController);
					break;
				case 2:
					delete(employeeController);
					break;
				case 3:
					update(employeeController);
					break;
				case 4:
					sort(employeeController);
					break;
				case 5:
					showEmployees(employeeController);
					break;
				case 0:
					return;
				default:
					System.out.println("Invalid Command");
			}
		}
	}

	public static void add(EmployeeController employeeController){
		Console console = null;

		try {
			Scanner sc = new Scanner(System.in);

			System.out.println("First Name: "); String firstName = sc.nextLine();
			System.out.println("Last Name: "); String lastName = sc.nextLine();
			System.out.println("Age: "); 			String age = sc.nextLine();
			System.out.println("CNP: "); String cnp = sc.nextLine();
			//System.out.println("Didactic Function: "); String didacticFunction = sc.nextLine();
			System.out.println("Salary: "); String salary = sc.nextLine();

			Employee employee = new Employee(firstName, lastName, age, cnp, DidacticFunction.ASISTANT, salary);
			employeeController.addEmployee(employee);
		} catch(Exception ex) {
			ex.printStackTrace();
		}

		mainMenu(employeeController);
	}

	public static void delete(EmployeeController employeeController){
		Console console = null;

		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("CNP: "); String cnp = sc.nextLine();

			employeeController.deleteEmployee(cnp);
		} catch(Exception ex) {
			ex.printStackTrace();
		}

		mainMenu(employeeController);
	}

	public static void update(EmployeeController employeeController){
		Console console = null;

		try {
			Scanner sc = new Scanner(System.in);

			System.out.println("CNP: "); String cnp = sc.nextLine();
			System.out.println("First Name: "); String firstName = sc.nextLine();
			System.out.println("Last Name: "); String lastName = sc.nextLine();
			System.out.println("Age: "); String age = sc.nextLine();
			System.out.println("Salary: "); String salary = sc.nextLine();

			employeeController.modifyEmployee(cnp, firstName, lastName, age, salary);
		} catch(Exception ex) {
			ex.printStackTrace();
		}

		mainMenu(employeeController);
	}

	public static void sort(EmployeeController employeeController){
		List<Employee> employees = employeeController.sortEmployees();
		for(Employee emplyee: employees){
			System.out.println(emplyee.toString());
		}

		mainMenu(employeeController);
	}

	public static void showEmployees(EmployeeController employeeController){
		List<Employee> employees = employeeController.getEmployeesList();
		for(Employee emplyee: employees){
			System.out.println(emplyee.toString());
		}

		mainMenu(employeeController);
	}

	public static void main(String[] args) {
		
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);

		mainMenu(employeeController);

//		for(Employee _employee : employeeController.getEmployeesList())
//			System.out.println(_employee.toString());
//		System.out.println("-----------------------------------------");
//
//		Employee employee = new Employee("FirstName", "LastName", "Age", "1234567894321", DidacticFunction.ASISTANT, "2500");
//		employeeController.addEmployee(employee);
//
//		for(Employee _employee : employeeController.getEmployeesList())
//			System.out.println(_employee.toString());
//
//		EmployeeValidator validator = new EmployeeValidator();
//		System.out.println( validator.isValid(new Employee("FirstName", "LastName", "Age", "1234567894322", DidacticFunction.TEACHER, "3400")) );
//
	}

}

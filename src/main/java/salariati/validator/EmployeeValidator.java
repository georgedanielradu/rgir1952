package salariati.validator;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public boolean isValid(Employee employee) {
		boolean isFirstNameValid = employee.getFirstName().matches("[a-zA-Z]+[ -]?[a-zA-Z]+") && (employee.getFirstName().length() > 2);
		boolean isLastNameValid  = employee.getLastName().matches("[a-zA-Z]+[ -]?[a-zA-Z]+") && (employee.getLastName().length() > 2);
		boolean isAgeValid       = employee.getAge().matches("[0-9]+") && (employee.getAge().length() > 1) && (Integer.parseInt(employee.getAge()) > 15);
		boolean isCNPValid       = employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTANT) ||
								   employee.getFunction().equals(DidacticFunction.LECTURER) ||
								   employee.getFunction().equals(DidacticFunction.TEACHER)  ||
				                   employee.getFunction().equals(DidacticFunction.ASSOCIATE_PROFESSOR);
		boolean isSalaryValid    = employee.getSalary().matches("[0-9]+") && (employee.getSalary().length() > 1) && (Integer.parseInt(employee.getSalary()) > 0);
		
		return isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid;
	}

	
}

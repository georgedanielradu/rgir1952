package salariati.enumeration;

public enum DidacticFunction {
	ASISTANT, LECTURER, TEACHER, ASSOCIATE_PROFESSOR;
}

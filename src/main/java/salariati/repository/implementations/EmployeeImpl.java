package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "./src/main/resources/employeeDB/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		List<Employee> employees = getEmployeeList();
		employees.remove(employee);

		try
		{
			PrintWriter writer = new PrintWriter(employeeDBFile);
			writer.print("");
			writer.close();
		}catch(Exception ex){}

		for(Employee employee1 : employees){
			addEmployee(employee1);
		}
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		List<Employee> employees = getEmployeeList();

		if(employeeValidator.isValid(newEmployee))
			Collections.replaceAll(employees, oldEmployee, newEmployee);

		try
		{
			PrintWriter writer = new PrintWriter(employeeDBFile);
			writer.print("");
			writer.close();
		}catch(Exception ex){}

		for(Employee employee1 : employees){
			addEmployee(employee1);
		}
	}

	@Override
	public Employee getEmployeeByCnp(String cnp) {
		List<Employee> employees = getEmployeeList();
		for(Employee employee: employees){
			if (employee.getCnp().equals(cnp))
				return employee;
		}
		return null;
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		return employeeList;
	}

}

package salariati.test;

import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Dani on 20-May-18.
 */
public class SortEmployeesTest {

    private final String employeeDBFile = "./src/main/resources/employeeDB/employees.txt";

    @Test
    public void test1(){
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        List<Employee> sortedEmployees = employeeController.sortEmployees();
        assert sortedEmployees.get(0).getFirstName().equals("first");
    }

    @Test
    public void test2(){
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        List<Employee> sortedEmployees = employeeController.sortEmployees();
        assert !sortedEmployees.get(0).getFirstName().equals("second");
    }
}

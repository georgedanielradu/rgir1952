package salariati.test;

import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.util.List;

/**
 * Created by Dani on 20-May-18.
 */
public class AddAnEmployeeTest {
    @Test
    public void test1(){
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        Employee empl = new Employee("empl", "empl", "50", "1111111111111", DidacticFunction.ASISTANT, "120");
        boolean wasAdded = employeesRepository.addEmployee(empl);
        assert wasAdded;
    }

    @Test
    public void test2(){
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        Employee empl = new Employee("", "", "", "", DidacticFunction.ASISTANT, "");
        boolean wasAdded = employeesRepository.addEmployee(empl);
        assert !wasAdded;
    }
}

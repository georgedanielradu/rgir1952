package salariati.test;

import org.junit.Test;

/**
 * Created by Dani on 20-May-18.
 */
public class IntegrareTopDown {
    @Test
    public void testA(){
        AddAnEmployeeTest testClassA = new AddAnEmployeeTest();
        testClassA.test1();
    }

    @Test
    public void testAB(){
        AddAnEmployeeTest testClassA = new AddAnEmployeeTest();
        testClassA.test1();
        ModifyEmployeeTest testClassB = new ModifyEmployeeTest();
        testClassB.test1();
    }

    @Test
    public void testABC(){
        SortEmployeesTest testClassC = new SortEmployeesTest();
        testClassC.test1();
        ModifyEmployeeTest testClassB = new ModifyEmployeeTest();
        testClassB.test1();
        AddAnEmployeeTest testClassA = new AddAnEmployeeTest();
        testClassA.test1();
    }
}

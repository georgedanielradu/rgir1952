package salariati.test;

import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.util.List;

/**
 * Created by Dani on 20-May-18.
 */
public class IntegrareBigBang {
    @Test
    public void testA(){
        AddAnEmployeeTest testClassA = new AddAnEmployeeTest();
        testClassA.test1();
    }

    @Test
    public void testB(){
        ModifyEmployeeTest testClassB = new ModifyEmployeeTest();
        testClassB.test1();
    }

    @Test
    public void testC(){
        SortEmployeesTest testClassC = new SortEmployeesTest();
        testClassC.test1();
    }

    @Test
    public void integrationTest(){
        AddAnEmployeeTest testClassA = new AddAnEmployeeTest();
        testClassA.test1();
        ModifyEmployeeTest testClassB = new ModifyEmployeeTest();
        testClassB.test1();
        SortEmployeesTest testClassC = new SortEmployeesTest();
        testClassC.test1();
    }
}

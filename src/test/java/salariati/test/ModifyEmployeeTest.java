package salariati.test;

import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.util.List;

/**
 * Created by Dani on 09-May-18.
 */
public class ModifyEmployeeTest {

    @Test
    public void test1(){
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        Employee oldEmpl = new Employee("ttt", "ttt", "34", "1111111111111", DidacticFunction.ASISTANT, "12");
        employeesRepository.addEmployee(oldEmpl);
        Employee newEmpl = new Employee("test", "test", "test", "1111111111111", DidacticFunction.ASISTANT, "23");
        employeesRepository.modifyEmployee(oldEmpl , newEmpl);
        List<Employee> employees = employeesRepository.getEmployeeList();
        assert employees.remove(employees.size()-1).equals(newEmpl);
    }

    @Test
    public void test2(){
        EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);
        Employee oldEmpl = new Employee("ttt", "ttt", "34", "1111111111111", DidacticFunction.ASISTANT, "12");
        employeesRepository.addEmployee(oldEmpl);
        Employee newEmpl = new Employee("test", "", "", "", DidacticFunction.ASISTANT, "");
        employeesRepository.modifyEmployee(oldEmpl , newEmpl);
        List<Employee> employees = employeesRepository.getEmployeeList();
        assert !employees.remove(employees.size()-1).equals(newEmpl);
    }
}
